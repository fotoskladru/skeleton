<?php
/**
 * @author primipilus 20.06.2016
 */
require __DIR__ . '/../../common/project.php';

$config = yii\helpers\ArrayHelper::merge(
    require Yii::getAlias('@fscore/common/config/main.php'),
    require __PROJECT_DIR__ . '/common/config/main.php',
    require __PROJECT_DIR__ . '/common/config/main-local.php',
    require Yii::getAlias('@fscore/welcome/config/main.php'),
    require __PROJECT_DIR__ . '/welcome/config/main.php',
    require __PROJECT_DIR__ . '/welcome/config/main-local.php'
);

$application = new yii\web\Application($config);
$application->run();