<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '{{staff.main.cookieValidationKey32:string(32)}}',
        ],
    ],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = ['class' => \yii\debug\Module::class];
}

return $config;