<?php
return [
    'rest'  => [
        'host'     => '{{common.params.rest.host}}',
        'username' => '{{common.params.rest.username}}',
        'password' => '{{common.params.rest.password}}',
    ],
    'exchange'  => [
        'host'     => '{{common.params.exchange.host}}',
        'username' => '{{common.params.exchange.username}}',
        'password' => '{{common.params.exchange.password}}',
    ],
    'admin' => [
        'username' => 'admin',
        'password' => '{{common.params.admin.password:string(32)}}',
    ],
    '1c'    => [
        'username' => 'exchange',
        'password' => '{{common.params.1c.password:string(32)}}',
    ],
    'supportEmail'        => 'fs.development@yandex.ru',
    'orderRecipientEmail' => 'fs.development@yandex.ru',
    'phoneCallbackMailTo' => 'fs.development@yandex.ru',
];