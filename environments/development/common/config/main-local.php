<?php
$config = [
    'components' => [
        'log'        => [
            'traceLevel'    => 3,
            'targets'       => [
                [
                    'class'   => \yii\log\FileTarget::class,
                    'levels'  => ['error', 'warning', 'info'],
                    'logVars' => [],
                ],
            ],
            'flushInterval' => 1,
        ],
        'db'         => [
            'class'             => \yii\db\Connection::class,
            'dsn'               => '{{common.main.db.dsn}}',
            'username'          => '{{common.main.db.username}}',
            'password'          => '{{common.main.db.password}}',
            'charset'           => 'utf8',
            'tablePrefix'       => 'dev_',
            'enableSchemaCache' => true,
        ],
        'cache'      => [
            'class'        => \yii\caching\MemCache::class,
            'useMemcached' => true,
            'keyPrefix'    => '{{common.main.cache.prefix}}' . YII_ENV,
            'servers'      => [
                [
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 60,
                ],
            ],
        ],
    ],
    'bootstrap'  => ['cache'],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = ['class' => \yii\gii\Module::class];
}

return $config;
