<?php
$config = [
    'components' => [
        'log'        => [
            'targets'       => [
                [
                    'class'   => \yii\log\FileTarget::class,
                    'levels'  => ['error', 'warning',],
                    'logVars' => [],
                ],
            ],
            'flushInterval' => 1,
        ],
        'db'         => [
            'class'             => \yii\db\Connection::class,
            'dsn'               => '{{common.main.db.dsn}}',
            'username'          => '{{common.main.db.username}}',
            'password'          => '{{common.main.db.password}}',
            'charset'           => 'utf8',
            'tablePrefix'       => 'stage_',
            'enableSchemaCache' => true,
        ],
        'cache'      => [
            'class'        => \yii\caching\MemCache::class,
            'useMemcached' => true,
            'keyPrefix'    => '{{common.main.cache.prefix}}' . YII_ENV,
            'servers'      => [
                [
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 60,
                ],
            ],
        ],
    ],
    'bootstrap'  => ['cache'],
];

return $config;
