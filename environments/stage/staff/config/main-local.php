<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '{{staff.main.cookieValidationKey32:string(32)}}',
        ],
    ],
];

return $config;