<?php
$config = [
    'components' => [
        'log'   => [
            'traceLevel'    => 3,
            'targets'       => [
                [
                    'class'   => \yii\log\FileTarget::class,
                    'levels'  => ['error',],
                    'logVars' => [],
                ],
            ],
            'flushInterval' => 1,
        ],
        'db'         => [
            'class'             => \yii\db\Connection::class,
            'dsn'               => '{{common.main.db.dsn}}',
            'username'          => '{{common.main.db.username}}',
            'password'          => '{{common.main.db.password}}',
            'charset'           => 'utf8',
            'tablePrefix'       => 'prod_',
            'enableSchemaCache' => true,
        ],
        'cache' => [
            'class'        => \yii\caching\MemCache::class,
            'useMemcached' => true,
            'keyPrefix'    => '{{common.main.cache.prefix}}' . YII_ENV,
            'servers'      => [
                [
                    'host'   => 'localhost',
                    'port'   => 11211,
                    'weight' => 60,
                ],
            ],
        ],
        'priceLogic' => [
            'class' => \fs\core\common\catalog\price\SingleLogic::class,
            'type'  => '{{common.main.price_logic_guid}}', // type price guid
        ],
        'sms'        => [
            'class'    => \fs\core\common\components\sms\Smsc::class,
            'url'      => 'https://smsc.ru/sys/send.php',
            'sender'   => 'Mobila.shop',
            'login'    => 'it@fotosklad.ru',
            'password' => '{{common.main.sms.password}}',
        ],
        'mailer'      => [
            'class'            => \yii\swiftmailer\Mailer::class,
            'viewPath'         => '@fscore/common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
    ],
    'bootstrap'  => ['cache'],
];


return $config;
