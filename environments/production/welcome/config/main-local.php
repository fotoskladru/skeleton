<?php

$config = [
    'components' => [
        'request' => [
            'cookieValidationKey' => '{{welcome.main.cookieValidationKey32:string(32)}}',
        ],
    ],
];

return $config;