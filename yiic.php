#!/usr/bin/env php7.1
<?php

require __DIR__ . '/common/project.php';

$config = yii\helpers\ArrayHelper::merge(
    require Yii::getAlias('@fscore/common/config/main.php'),
    require __PROJECT_DIR__ . '/common/config/main.php',
    require __PROJECT_DIR__ . '/common/config/main-local.php',
    require Yii::getAlias('@fscore/console/config/main.php'),
    require __PROJECT_DIR__ . '/console/config/main.php',
    require __PROJECT_DIR__ . '/console/config/main-local.php'
);

$application = new yii\console\Application($config);
$exitCode = $application->run();
exit($exitCode);