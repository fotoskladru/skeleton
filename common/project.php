<?php
/**
 * @author primipilus 20.06.2016
 */

require __DIR__ . '/../local.php';

defined('YII_ENV_STAGE') or define('YII_ENV_STAGE', YII_ENV === 'stage');

require __PROJECT_DIR__ . '/vendor/autoload.php';
require __PROJECT_DIR__ . '/vendor/yiisoft/yii2/Yii.php';

if (YII_ENV_DEV) {
    require __PROJECT_DIR__ . '/devpackages/autoloader.php';
}

require __PROJECT_DIR__ . '/common/config/bootstrap.php';