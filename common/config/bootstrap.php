<?php

Yii::setAlias('@vendor', __PROJECT_DIR__ . '/vendor');
Yii::setAlias('@common', __PROJECT_DIR__ . '/common');
Yii::setAlias('@welcome', __PROJECT_DIR__ . '/welcome');
Yii::setAlias('@staff', __PROJECT_DIR__ . '/staff');
Yii::setAlias('@console', __PROJECT_DIR__ . '/console');
Yii::setAlias('@modules', __PROJECT_DIR__ . '/modules');

if (YII_ENV_DEV && file_exists(__PROJECT_DIR__ . '/devpackages/core')) {
    Yii::setAlias('@fscore', __PROJECT_DIR__ . '/devpackages/core/src');
} else {
    Yii::setAlias('@fscore', '@vendor/fotoskladru/core/src');
}
Yii::setAlias('@fs/core', '@fscore');

Yii::setAlias('@welcomeWebRoot', WELCOME_WEB_ROOT);
Yii::setAlias('@staffWebRoot', STAFF_WEB_ROOT);

Yii::setAlias('@bower_components', __PROJECT_DIR__ . '/bower_components');