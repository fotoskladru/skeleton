<?php
$params = array_merge(
    require Yii::getAlias('@fscore/common/config/params.php'),
    require __PROJECT_DIR__ . '/common/config/params.php',
    require __PROJECT_DIR__ . '/common/config/params-local.php',
    require Yii::getAlias('@fscore/console/config/params.php'),
    require __DIR__ . '/params.php'
);

$config = [
    //'controllerMap'       => require __DIR__ . '/controllersMap.php',
    'components'          => [

    ],
    'params'              => $params,
];

return $config;